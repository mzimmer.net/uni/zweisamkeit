﻿using UnityEngine;

public class Missile : MonoBehaviour
{
	public float initialThrust = 2000.0f;
	public float damageOnImpact = 100.0f;
	public float maxLifeTime = 20.0f;

	public ParticleSystem impactAnimation;
	public AudioSource explodeSound;

	void Start ()
	{
		GetComponent<Rigidbody> ().AddForce (transform.forward * initialThrust);
		Destroy (gameObject, maxLifeTime);
	}

	void OnTriggerEnter (Collider other)
	{
		Health health = other.GetComponent<Health> ();
		if (health != null) {
			other.GetComponent<Health> ().TakeDamage (damageOnImpact);
		}
		impactAnimation.Play ();
		explodeSound.pitch = Random.value / 5.0f + 0.8f;
		explodeSound.Play ();
		Destroy (gameObject, impactAnimation.duration + 1.0f);
	}
}
