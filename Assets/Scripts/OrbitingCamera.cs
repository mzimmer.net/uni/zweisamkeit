﻿using UnityEngine;

public class OrbitingCamera : MonoBehaviour
{
	public float rotateSpeed = 50.0f;
	public float zoomSpeed = 1200.0f;
	public float distance = 70.0f;
	public float minDistance = 1.0f;
	public float currentAngleX = 0.0f;
	public float currentAngleY = 0.0f;

	private Transform anchor;
	private bool forceLockedCursorLockState = false;

	public void Setup (Transform anchor)
	{
		this.anchor = anchor;
	}

	void Update ()
	{
		if (anchor != null) {
			if (Input.GetMouseButtonDown (1) || Input.GetMouseButtonUp (1)) {
				SetCursorMode ();
			}
			distance += Input.GetAxis ("Mouse ScrollWheel") * -zoomSpeed * Time.deltaTime;
			if (distance < minDistance) {
				distance = minDistance;
			}
			if (Cursor.lockState == CursorLockMode.Locked) {
				currentAngleX += Input.GetAxis ("Mouse X") * rotateSpeed * Time.deltaTime;
				currentAngleY += Input.GetAxis ("Mouse Y") * -rotateSpeed * Time.deltaTime;
			}
			Quaternion q = Quaternion.Euler (currentAngleY, currentAngleX, 0);
			Quaternion relRotaion = anchor.rotation * q;
			Vector3 direction = relRotaion * Vector3.forward;
			transform.position = anchor.position - direction * distance;
			transform.LookAt (anchor.position, anchor.TransformVector (Vector3.up));
		}
	}

	public void SetMode (bool forceLockedCursorLockState)
	{
		this.forceLockedCursorLockState = forceLockedCursorLockState;
		SetCursorMode ();
	}

	private void SetCursorMode ()
	{
		if (forceLockedCursorLockState || Input.GetMouseButton (1)) {
			Cursor.lockState = CursorLockMode.Locked;
		} else {
			Cursor.lockState = CursorLockMode.None;
		}
		Cursor.visible = (CursorLockMode.Locked != Cursor.lockState);
	}
}
