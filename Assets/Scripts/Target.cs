﻿using UnityEngine;
using UnityEngine.Networking;

public class Target : NetworkBehaviour
{
	[SyncVar][HideInInspector]
	public GameObject metaShipGO = null;

	public MetaShip MetaShip {
		get {
			return metaShipGO == null ? null : metaShipGO.GetComponent<MetaShip> ();
		}
		set {
			metaShipGO = value == null ? null : value.gameObject;
		}
	}

	public override void OnStartAuthority ()
	{
		GetComponent<Gunner> ().enabled = true;
	}

	public override void OnStopAuthority ()
	{
		GetComponent<Gunner> ().enabled = false;
	}
}
