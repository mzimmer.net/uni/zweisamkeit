﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

public class Debugger : NetworkBehaviour
{
	public Text text;

	void Update ()
	{
		string t;
		List<NetworkConnection> ncs = new List<NetworkConnection> ();
		if (isServer) {
			t = "I am the server. Connections:\n";
			ncs.AddRange (NetworkServer.connections);
		} else {
			t = "I am a client. Connections:\n";
			foreach (NetworkClient nc in NetworkClient.allClients) {
				ncs.Add (nc.connection);
			}
		}
		foreach (NetworkConnection nc in ncs) {
			if (nc != null) {
				t += "    [" + nc.connectionId + "].playerController:\n";
				foreach (PlayerController pc in nc.playerControllers) {
					Player p = pc.gameObject.GetComponent<Player> ();
					if (p != null) {
						t += "        " + p.ToString () + "\n";
						if (pc.unetView.isLocalPlayer) {
							t += "        ^^^^ THIS IS ME ^^^^\n";
						}
					}
				}
				t += "    [" + nc.connectionId + "].clientOwnedObjects:\n";
				if (nc.clientOwnedObjects != null) {
					foreach (NetworkInstanceId niid in nc.clientOwnedObjects) {
						t += "        " + niid.Value + ": " + ClientScene.FindLocalObject (niid).name + "\n";
					}
				}
			}
		}

		t += "\n\n\nLocalNetworkedObjects:\n";
		object[] obj = GameObject.FindObjectsOfType (typeof(GameObject));
		foreach (object o in obj) {
			GameObject go = (GameObject)o;
			NetworkIdentity ni = go.GetComponent<NetworkIdentity> ();
			if (ni != null) {
				t += go.name + ": " + ni.hasAuthority + "\n";
			}
		}

		t += "\n\n\nAlive: " + Player.localPlayer.MetaShip.alive + "\n";

		text.text = t;
	}
}
