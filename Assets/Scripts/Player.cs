﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Player : NetworkBehaviour
{
	public enum Task
	{
		NAVIGATOR,
		GUNNER
	}

	public static Player localPlayer = null;

	public Sprite youWin;
	public Sprite youLose;
	public Texture2D healthbarEmpty;
	public Texture2D healthbarFull;
	public Vector2 pos = new Vector2 (20, 800);
	public Vector2 size = new Vector2 (600, 72);

	[SyncVar][HideInInspector]
	public string playerName;

	[SyncVar][HideInInspector]
	public Task task;

	[SyncVar][HideInInspector]
	public GameObject metaShipGO = null;

	public MetaShip MetaShip {
		get {
			return metaShipGO == null ? null : metaShipGO.GetComponent<MetaShip> ();
		}
		set {
			metaShipGO = value == null ? null : value.gameObject;
		}
	}

	private OrbitingCamera orbitingCamera;
	private Image youWinLoseHolder;

	public override void OnStartServer ()
	{
		if (task == Task.NAVIGATOR) {
			MetaShip.NavigatorPlayer = this;
		} else { /* if (task == Task.GUNNER) */
			MetaShip.GunnerPlayer = this;
		}
	}

	public override void OnStartLocalPlayer ()
	{
		localPlayer = this;

		if (task == Task.NAVIGATOR) {
			TakeAuthority (MetaShip.shipGO);
		} else { /* if (task == Task.GUNNER) */
			TakeAuthority (MetaShip.targetGO);
		}

		orbitingCamera = GameObject.Find ("Camera").GetComponent<OrbitingCamera> ();
		youWinLoseHolder = GameObject.Find ("YouWinLoseHolder").GetComponent<Image> ();

		if (task == Task.NAVIGATOR) {

			orbitingCamera.Setup (MetaShip.shipGO.transform);

		} else { /* if (task == Task.GUNNER) */
			orbitingCamera.SetMode (true);
			Transform gunnerCameraAnchorTransform = new GameObject ().transform;
			gunnerCameraAnchorTransform.parent = MetaShip.shipGO.transform;
			gunnerCameraAnchorTransform.localPosition = MetaShip.gunnerCameraAnchorOffset;
			orbitingCamera.Setup (gunnerCameraAnchorTransform);
			MetaShip.targetGO.GetComponent<Gunner> ().orbitingCamera = orbitingCamera;

		}
	}

	void OnGUI ()
	{
		if (isLocalPlayer) {
			Debug.Log ("Drawing healthbar");
			GUI.BeginGroup (new Rect (pos.x, pos.y, size.x, size.y));
			{
				GUI.Box (new Rect (0, 0, size.x, size.y), healthbarEmpty);
				GUI.BeginGroup (new Rect (0, 0, size.x * (MetaShip.Ship.health / 1000.0f), size.y));
				{
					GUI.Box (new Rect (0, 0, size.x, size.y), healthbarFull);
				}
				GUI.EndGroup ();
			}
			GUI.EndGroup ();
		}
	}

	public void TakeAuthority (GameObject go)
	{
		if (isLocalPlayer) {
			CmdTakeAuthority (go.GetComponent<NetworkIdentity> ());
		}
	}

	[Command]
	void CmdTakeAuthority (NetworkIdentity ni)
	{
		ni.AssignClientAuthority (connectionToClient);
	}

	public void ReleaseAuthority (GameObject go)
	{
		if (isLocalPlayer) {
			CmdReleaseAuthority (go.GetComponent<NetworkIdentity> ());
		}
	}

	[Command]
	void CmdReleaseAuthority (NetworkIdentity ni)
	{
		ni.RemoveClientAuthority (connectionToClient);
	}

	[ClientRpc]
	public void RpcYouLose ()
	{
		if (isLocalPlayer) {
			orbitingCamera.Setup (transform);
			orbitingCamera.SetMode (false);
			youWinLoseHolder.sprite = youLose;
			youWinLoseHolder.enabled = true;
		}
	}

	[ClientRpc]
	public void RpcYouWin ()
	{
		if (isLocalPlayer) {
			orbitingCamera.SetMode (false);
			youWinLoseHolder.sprite = youWin;
			youWinLoseHolder.enabled = true;
			if (task == Task.NAVIGATOR) {
				MetaShip.shipGO.GetComponent<Navigator> ().enabled = false;
			} else { /* if (task == Task.GUNNER) */
				MetaShip.targetGO.GetComponent<Gunner> ().enabled = false;
			}
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Player: Name={0}, Team={1}, Task={2}]", playerName, MetaShip.team, task.ToString ());
	}
}
