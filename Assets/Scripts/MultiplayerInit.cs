﻿using UnityEngine;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using System.Collections.Generic;

public class MultiplayerInit : LobbyHook
{
	public Vector3 gunnerCameraAnchorOffset;
	public GameObject metaShipPrefab;
	public GameObject shipPrefab;
	public GameObject turretPrefab;
	public GameObject targetPrefab;
	public GameObject[] asteroidPrefabs;

	private int playersHandled;
	private Vector3[] spawnPositions;
	private GameManager gameManager;

	void Start ()
	{
		playersHandled = 0;
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	public override void OnLobbyServerSceneLoadedForPlayer (UnityEngine.Networking.NetworkManager manager, GameObject lobbyPlayerGO, GameObject playerGO)
	{
		if (spawnPositions == null) {
			spawnPositions = new Vector3[manager.numPlayers];
			for (int i = 0; i < spawnPositions.Length; i++) {
				spawnPositions [i] = Vector3.right * 300.0f * i;
			}
		}

		LobbyPlayer lobbyPlayer = lobbyPlayerGO.GetComponent<LobbyPlayer> ();
		Player player = playerGO.GetComponent<Player> ();

		Dictionary<Color, int> teams = new Dictionary<Color, int> () {
			{ Color.red, 1 },
			{ Color.cyan, 1 },
			{ Color.blue, 2 },
			{ Color.green, 2 },
			{ Color.yellow, 3 },
			{ Color.magenta, 3 }
		};

		Dictionary<Color, Player.Task> tasks = new Dictionary<Color, Player.Task> () {
			{ Color.red, Player.Task.NAVIGATOR },
			{ Color.cyan, Player.Task.GUNNER },
			{ Color.blue, Player.Task.NAVIGATOR },
			{ Color.green, Player.Task.GUNNER },
			{ Color.yellow, Player.Task.NAVIGATOR },
			{ Color.magenta, Player.Task.GUNNER }
		};

		player.playerName = lobbyPlayer.playerName;
		player.task = tasks [lobbyPlayer.playerColor];
		int team = teams [lobbyPlayer.playerColor];

		GameObject metaShipGO;
		MetaShip metaShip;
		GameObject shipGO;
		Ship ship;
		GameObject targetGO;
		Target target;
		if (gameManager.ships.TryGetValue (team, out metaShip)) {
			metaShipGO = metaShip.gameObject;
			ship = metaShip.Ship;
			target = metaShip.Target;
			shipGO = ship.gameObject;
			targetGO = target.gameObject;
		} else {
			metaShipGO = (GameObject)Instantiate (metaShipPrefab, Vector3.zero, Quaternion.identity);
			shipGO = (GameObject)Instantiate (shipPrefab, spawnPositions [playersHandled], Quaternion.identity);
			targetGO = (GameObject)Instantiate (targetPrefab, Vector3.zero, Quaternion.identity);
			NetworkServer.Spawn (metaShipGO);
			NetworkServer.Spawn (shipGO);
			NetworkServer.Spawn (targetGO);

			metaShip = metaShipGO.GetComponent<MetaShip> ();
			ship = shipGO.GetComponent<Ship> ();
			target = targetGO.GetComponent<Target> ();

			foreach (Transform childTransform in ship.transform) {
				if (childTransform.gameObject.name.StartsWith ("TurretAnchor")) {
					GameObject turretGO = (GameObject)Instantiate (turretPrefab);
					if (turretGO != null) {
						Turret turret = turretGO.GetComponent<Turret> ();
						turret.initParentGO = shipGO;
						turret.initPosition = childTransform.localPosition;
						turret.initRotation = childTransform.localRotation;
						turret.initTarget = targetGO;
						NetworkServer.Spawn (turretGO);
					}
					Destroy (childTransform.gameObject);
				}
			}

			metaShip.team = team;
			metaShip.alive = true;
			metaShip.Ship = ship;
			metaShip.gunnerCameraAnchorOffset = gunnerCameraAnchorOffset;
			metaShip.Target = target;
			ship.MetaShip = metaShip;
			target.MetaShip = metaShip;

			gameManager.ships.Add (team, metaShip);
		}

		if (team == metaShip.team) {
			player.MetaShip = metaShip;
		}

		playersHandled++;


		if (gameManager.ships.Count == playersHandled) {
			int n = 200;
			float minSize = 5;
			float maxSize = 30;
			float spawnRadius = 1000.0f;
			for (int i = 0; i < n; i++) {
				GameObject asteroidPrefab = asteroidPrefabs [Random.Range (0, asteroidPrefabs.Length)];
				Vector3 position;
				do {
					position = Util.randomNormalizedVector3 () * Random.value * spawnRadius;
				} while(insideSpheres (position, spawnPositions, 50.0f));
				GameObject asteroidGO = (GameObject)Instantiate (asteroidPrefab, position, Random.rotation);
				Asteroid asteroid = asteroidGO.GetComponent<Asteroid> ();
				asteroid.Setup (Random.value * (maxSize - minSize) + minSize);
				NetworkServer.Spawn (asteroidGO);
			}
		}
	}

	public override void OnStopHost (NetworkManager manager)
	{
		playersHandled = 0;
		gameManager.ships.Clear ();
	}

	private bool insideSpheres (Vector3 position, Vector3[] sphereCenters, float radius)
	{
		foreach (Vector3 sphereCenter in sphereCenters) {
			if (Vector3.Distance (position, sphereCenter) < radius) {
				return true;
			}
		}
		return false;
	}
}
