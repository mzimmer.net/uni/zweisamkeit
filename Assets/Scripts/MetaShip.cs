﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class MetaShip : NetworkBehaviour
{
	[SyncVar]
	public int team = 0;

	[SyncVar]
	public bool alive = false;

	[SyncVar]
	public GameObject shipGO = null;

	public Ship Ship {
		get {
			return shipGO == null ? null : shipGO.GetComponent<Ship> ();
		}
		set {
			shipGO = value == null ? null : value.gameObject;
		}
	}

	[SyncVar]
	public GameObject navigatorPlayerGO = null;

	public Player NavigatorPlayer {
		get {
			return navigatorPlayerGO == null ? null : navigatorPlayerGO.GetComponent<Player> ();
		}
		set {
			navigatorPlayerGO = value == null ? null : value.gameObject;
		}
	}

	[SyncVar]
	public Vector3 gunnerCameraAnchorOffset = Vector3.zero;

	[SyncVar]
	public GameObject targetGO = null;

	public Target Target {
		get {
			return targetGO == null ? null : targetGO.GetComponent<Target> ();
		}
		set {
			targetGO = value == null ? null : value.gameObject;
		}
	}

	[SyncVar]
	public GameObject gunnerPlayerGO = null;

	public Player GunnerPlayer {
		get {
			return gunnerPlayerGO == null ? null : gunnerPlayerGO.GetComponent<Player> ();
		}
		set {
			gunnerPlayerGO = value == null ? null : value.gameObject;
		}
	}
}
