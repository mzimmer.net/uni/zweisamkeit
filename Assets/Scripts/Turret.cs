﻿using UnityEngine;
using UnityEngine.Networking;

public class Turret : NetworkBehaviour
{
	public float reloadTime = 1.0f;
	public float maxAimingDistance = 35.0f;

	public float rotateSpeed = 50.0f;
	public Transform guard;
	public Vector3 guardAxis;
	public Vector3 guardMiddlePosition;
	public float guardMax;
	public Transform barrel;
	public Vector3 barrelAxis;
	public Vector3 barrelMiddlePosition;
	public float barrelMax;
	public Transform muzzle;
	public ParticleSystem muzzleAnimation;
	public GameObject missilePrefab;
	public AudioSource fireSound;

	[SyncVar][HideInInspector]
	public GameObject initParentGO;
	[SyncVar][HideInInspector]
	public Vector3 initPosition;
	[SyncVar][HideInInspector]
	public Quaternion initRotation;
	[SyncVar][HideInInspector]
	public GameObject initTarget;

	private Transform target;
	private Quaternion guardMiddleDirection;
	private Quaternion barrelMiddleDirection;
	private float reloadTimer = 0.0f;

	void Start ()
	{
		guardAxis = guardAxis.normalized;
		barrelAxis = barrelAxis.normalized;

		Vector3 guardMiddlePositionOnPlane = Vector3.ProjectOnPlane (guardMiddlePosition, guardAxis);
		guardMiddleDirection = Quaternion.LookRotation (guardMiddlePositionOnPlane);

		Vector3 barrelMiddlePositionOnPlane = Vector3.ProjectOnPlane (barrelMiddlePosition, barrelAxis);
		barrelMiddleDirection = Quaternion.LookRotation (barrelMiddlePositionOnPlane);
	}

	public override void OnStartClient ()
	{
		base.OnStartClient ();
		transform.parent = initParentGO.transform;
		transform.localPosition = initPosition;
		transform.localRotation = initRotation;
		this.target = initTarget.transform;
	}

	void Update ()
	{
		if (target != null) {
			// Store current rotation
			Quaternion currentGuardRotation = guard.localRotation;
			Quaternion currentBarrelRotation = barrel.localRotation;

			// Reset rotation
			guard.localRotation = Quaternion.identity;
			barrel.localRotation = Quaternion.identity;

			// Position and direction of target in aimable plane of guard
			Vector3 guardTargetPosition = guard.InverseTransformPoint (target.position);
			Vector3 guardTargetPositionOnPlane = Vector3.ProjectOnPlane (guardTargetPosition, guardAxis);
			Quaternion guardDirection = Quaternion.LookRotation (guardTargetPositionOnPlane);

			// Set guards rotation to calculate barrels rotation
			guard.localRotation = guardDirection;

			// Position and direction of target in aimable plane of barrel
			Vector3 barrelTargetPosition = barrel.InverseTransformPoint (target.position);
			Vector3 barrelTargetPositionOnPlane = Vector3.ProjectOnPlane (barrelTargetPosition, barrelAxis);
			Quaternion barrelDirection = Quaternion.LookRotation (barrelTargetPositionOnPlane);

			// Set barrels rotation to calculate [no child = not needed] rotation
			// barrel.localRotation = relBarrelDirection;

			// Rotation boundaries
			guardDirection = Quaternion.RotateTowards (guardMiddleDirection, guardDirection, guardMax);
			barrelDirection = Quaternion.RotateTowards (barrelMiddleDirection, barrelDirection, barrelMax);

			// Actually rotate guard and barrel
			guard.localRotation = Quaternion.RotateTowards (currentGuardRotation, guardDirection, rotateSpeed * Time.deltaTime);
			barrel.localRotation = Quaternion.RotateTowards (currentBarrelRotation, barrelDirection, rotateSpeed * Time.deltaTime);
		}

		if (reloadTimer > 0.0f) {
			reloadTimer -= Time.deltaTime;
		}
	}

	[Command]
	public void CmdFireAtWill ()
	{
		if (reloadTimer <= 0.0f) {
			GameObject missile = (GameObject)Instantiate (missilePrefab, muzzle.position, muzzle.rotation);
			NetworkServer.Spawn (missile);
			RpcFireAtWillMuzzleAnimation ();
			reloadTimer = reloadTime;
		}
	}

	[ClientRpc]
	void RpcFireAtWillMuzzleAnimation ()
	{
		muzzleAnimation.Play ();
		fireSound.pitch = Random.value / 5.0f + 0.9f;
		fireSound.Play ();
	}
}
