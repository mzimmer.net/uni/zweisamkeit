﻿using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour
{
	[SyncVar]
	public float health = 1000.0f;
	public float collisionDamageFactor = 0.1f;

	public GameObject dieAnimationGO;
	public GameObject dieSoundGO;

	void OnTriggerEnter (Collider other)
	{
		Debug.Log ("Enter");
		Health otherHealth = other.GetComponent<Health> ();
		if (otherHealth != null) {
			Debug.Log ("Enter: Health");
			Rigidbody rb = gameObject.GetComponent<Rigidbody> ();
			Rigidbody otherRB = other.GetComponent<Rigidbody> ();
			if (rb != null && otherRB != null) {
				float damage = (rb.velocity - otherRB.velocity).magnitude * transform.localScale.magnitude * collisionDamageFactor;
				Debug.Log ("Enter: damage: " + damage);
				otherHealth.TakeDamage (damage);
			}
		}
	}

	[ContextMenu ("Health.Kill")]
	public void Kill ()
	{
		TakeDamage (health);
	}

	public void TakeDamage (float healthPoints)
	{
		if (!hasAuthority) {
			return;
		}

		health -= healthPoints;
		if (health <= 0.0f) {
			OnDie ();
		}
	}

	protected virtual void OnDie ()
	{
		if (netId.IsEmpty ()) {
			Destroy (gameObject);
		} else {
			RpcDie ();
		}
	}

	protected virtual float ParticleScale ()
	{
		return 1.0f;
	}

	[ClientRpc]
	protected void RpcDie ()
	{
		if (dieAnimationGO != null) {
			GameObject go = (GameObject)Instantiate (dieAnimationGO, transform.position, transform.rotation);
			go.transform.localScale *= ParticleScale ();
			float keepAlive = 0.0f;
			ParticleSystem[] dieAnimations = go.GetComponentsInChildren<ParticleSystem> ();
			foreach (ParticleSystem dieAnimation in dieAnimations) {
				dieAnimation.Play ();
				if (dieAnimation.duration > keepAlive) {
					keepAlive = dieAnimation.duration;
				}
			}
			Destroy (go, keepAlive);
		}

		if (dieSoundGO != null) {
			GameObject go = (GameObject)Instantiate (dieSoundGO, transform.position, transform.rotation);
			float keepAlive = 0.0f;
			AudioSource[] dieSounds = go.GetComponentsInChildren<AudioSource> ();
			foreach (AudioSource dieSound in dieSounds) {
				dieSound.Play ();
				if (dieSound.clip.length > keepAlive) {
					keepAlive = dieSound.clip.length;
				}
			}
			Destroy (go, keepAlive + 1.0f);
		}

		Destroy (gameObject);
	}
}
