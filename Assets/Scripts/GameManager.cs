﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	public Dictionary<int, MetaShip> ships;

	public GameManager ()
	{
		ships = new Dictionary<int, MetaShip> ();
	}

	void Start ()
	{
		DontDestroyOnLoad (gameObject);
	}

	public void WeDied (int team)
	{
		MetaShip metaShip;

		if (ships.TryGetValue (team, out metaShip)) {
			metaShip.alive = false;
			if (metaShip.navigatorPlayerGO != null) {
				metaShip.NavigatorPlayer.RpcYouLose ();
			}
			if (metaShip.gunnerPlayerGO != null) {
				metaShip.GunnerPlayer.RpcYouLose ();
			}
		} else {
			Debug.LogError ("Team " + team + " said, they died, but they are not registered!");
		}

		int aliveCounter = 0;
		foreach (MetaShip metaShip2 in ships.Values) {
			if (metaShip2.alive) {
				aliveCounter++;
				metaShip = metaShip2;
			}
		}
		if (aliveCounter == 1) {
			if (metaShip.navigatorPlayerGO != null) {
				metaShip.NavigatorPlayer.RpcYouWin ();
			}
			if (metaShip.gunnerPlayerGO != null) {
				metaShip.GunnerPlayer.RpcYouWin ();
			}
		}
	}
}
