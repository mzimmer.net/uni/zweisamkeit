﻿using UnityEngine;
using UnityEngine.Networking;

public class Ship : Health
{
	public AudioSource engineSound;
	public AudioSource asteroidCrashSound;

	[SyncVar][HideInInspector]
	public GameObject metaShipGO = null;
	private Rigidbody rb;

	public MetaShip MetaShip {
		get {
			return metaShipGO == null ? null : metaShipGO.GetComponent<MetaShip> ();
		}
		set {
			metaShipGO = value == null ? null : value.gameObject;
		}
	}

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void Update ()
	{
		engineSound.pitch = rb.velocity.magnitude / 32.0f + 0.2f;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<Asteroid> () != null) {
			float maxedVelocity = Mathf.Min (30.0f, (rb.velocity - other.attachedRigidbody.velocity).magnitude);
			float velocityEffect = maxedVelocity / 60.0f - 0.5f;
			float massEffect = other.attachedRigidbody.mass / -50.0f + 0.1f;
			asteroidCrashSound.pitch = 1.0f + massEffect + velocityEffect;
			asteroidCrashSound.volume = 1.0f + velocityEffect;
			asteroidCrashSound.Play ();
		}
	}

	public override void OnStartAuthority ()
	{
		GetComponent<Navigator> ().enabled = true;
	}

	public override void OnStopAuthority ()
	{
		GetComponent<Navigator> ().enabled = false;
	}

	void OnDestroy ()
	{
		if (!isServer) {
			return;
		}

		GameObject.Find ("GameManager").GetComponent<GameManager> ().WeDied (MetaShip.team);
		Destroy (MetaShip.targetGO);
	}
}
