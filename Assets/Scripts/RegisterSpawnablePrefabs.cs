﻿using UnityEngine;
using UnityEngine.Networking;

public class RegisterSpawnablePrefabs : MonoBehaviour
{
	public GameObject[] prefabs;

	void Awake ()
	{
		foreach (GameObject go in prefabs) {
			ClientScene.RegisterPrefab (go);
		}
	}
}
