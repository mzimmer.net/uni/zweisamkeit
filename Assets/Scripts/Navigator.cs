﻿using UnityEngine;

public class Navigator : MonoBehaviour
{
	public float forwardSpeed = 1000.0f;
	public float sidewaySpeed = 500.0f;
	public float yawPitchRotateSpeed = 2500.0f;
	public float rollRotateSpeed = 750.0f;

	public float currentSpeed = 0.0f;
	public float breakSpeed = 1.0f;

	private Rigidbody rb;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		rb.inertiaTensorRotation = Quaternion.identity;
	}

	void Update ()
	{
		if (Input.GetKey ("w")) {
			rb.AddTorque (transform.right * yawPitchRotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("s")) {
			rb.AddTorque (transform.right * -yawPitchRotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("q")) {
			rb.AddTorque (transform.forward * rollRotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("e")) {
			rb.AddTorque (transform.forward * -rollRotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("d")) {
			rb.AddTorque (transform.up * yawPitchRotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("a")) {
			rb.AddTorque (transform.up * -yawPitchRotateSpeed * Time.deltaTime);
		}

		if (Input.GetKey ("left shift")) {
			rb.AddRelativeForce (Vector3.forward * forwardSpeed * Time.deltaTime);
		} else if (Input.GetKey ("left ctrl")) {
			rb.AddRelativeForce (Vector3.forward * -forwardSpeed * Time.deltaTime);
		}
		if (Input.GetKey ("space")) {
			rb.velocity = Vector3.Slerp (rb.velocity, Vector3.zero, breakSpeed * Time.deltaTime);
		}

		if (Input.GetKey ("up")) {
			rb.AddRelativeForce (Vector3.up * sidewaySpeed * Time.deltaTime);
		}
		if (Input.GetKey ("down")) {
			rb.AddRelativeForce (Vector3.up * -sidewaySpeed * Time.deltaTime);
		}
		if (Input.GetKey ("right")) {
			rb.AddRelativeForce (Vector3.right * sidewaySpeed * Time.deltaTime);
		}
		if (Input.GetKey ("left")) {
			rb.AddRelativeForce (Vector3.right * -sidewaySpeed * Time.deltaTime);
		}
	}
}
