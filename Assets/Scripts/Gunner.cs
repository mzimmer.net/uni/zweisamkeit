﻿using UnityEngine;
using UnityEngine.UI;

public class Gunner : MonoBehaviour
{
	public float defaultDistance = 200.0f;

	public Sprite visorHit;
	public Sprite visorVoid;

	[HideInInspector]
	public OrbitingCamera orbitingCamera;
	
	private Image visorHolder;

	private Ray targetRay;
	private RaycastHit targetRaycastHit;
	private Turret[] turrets;

	public Turret[] Turrets {
		get {
			if (turrets == null) {
				turrets = GetComponent<Target> ().MetaShip.shipGO.GetComponentsInChildren<Turret> ();
				foreach (Turret turret in turrets) {
					Player.localPlayer.TakeAuthority (turret.gameObject);
				}
			}
			return turrets;
		}
	}

	private Collider[] colliders;

	public Collider[] Colliders {
		get {
			if (colliders == null) {
				colliders = GetComponent<Target> ().MetaShip.shipGO.GetComponents<Collider> ();
			}
			return colliders;
		}
	}

	void Start ()
	{
		visorHolder = GameObject.Find ("VisorHolder").GetComponent<Image> ();
		visorHolder.enabled = true;
	}

	void Update ()
	{
		if (Input.GetMouseButton (0)) {
			foreach (Turret turret in Turrets) {
				turret.CmdFireAtWill ();
			}
		}
	}

	void LateUpdate ()
	{
		if (orbitingCamera != null) {
			targetRay.origin = orbitingCamera.transform.position;
			targetRay.direction = orbitingCamera.transform.forward;

			RaycastHit hit;
			if (Physics.Raycast (targetRay, out hit) && notShootingInTheOwnKnee (hit)) {
				transform.position = hit.point;
				visorHolder.sprite = visorHit;
			} else {
				transform.position = targetRay.GetPoint (defaultDistance);
				visorHolder.sprite = visorVoid;
			}
		}
	}

	private bool notShootingInTheOwnKnee (RaycastHit hit)
	{
		foreach (Collider collider in Colliders) {
			if (hit.collider.Equals (collider)) {
				return false;
			}
		}
		return true;
	}

	void OnDestroy ()
	{
		if (visorHolder != null) {
			visorHolder.enabled = false;
		}
	}
}
