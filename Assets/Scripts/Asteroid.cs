﻿using UnityEngine;
using UnityEngine.Networking;

public class Asteroid : Health
{
	public GameObject[] asteroidPrefabs;
	public Transform[] spawnPoints;
	public float minimumSizeToSpawnSubAsteroids = 2.0f;
	public float massFactor = 1.0f;
	public float healthFactor = 20.0f;
	public float initialThrustFactor = 200.0f;

	[SyncVar]
	private Vector3 localScale;

	protected override void OnDie ()
	{
		if (!isServer) {
			return;
		}

		if (transform.localScale.magnitude >= minimumSizeToSpawnSubAsteroids) {
			SpawnSubAsteroids ();
		}

		base.OnDie ();
	}

	protected override float ParticleScale ()
	{
		return localScale.magnitude*2;
	}

	private void SpawnSubAsteroids ()
	{
		// spawn at least 2 and at most the number of spawnspoints
		int n = Random.Range (2, spawnPoints.Length + 1);

		// scale is parent scale devided by number of sub asteroids
		float scale = transform.localScale.magnitude / n;

		foreach (Transform spawnPoint in spawnPoints) {
			GameObject subAsteroidPrefab = asteroidPrefabs [Random.Range (0, asteroidPrefabs.Length)];
			GameObject subAsteroidGO = (GameObject)Instantiate (subAsteroidPrefab, spawnPoint.position, Random.rotation);
			Asteroid subAsteroid = subAsteroidGO.GetComponent<Asteroid> ();
			subAsteroid.Setup (scale);

			// split sub asteroid again with a chance of 10%
			if (Random.value > 0.9f) {
				Kill ();
			} else {
				NetworkServer.Spawn (subAsteroidGO);
			}
		}
	}

	public void Setup (float scale)
	{
		Vector3 initialForce = Util.randomNormalizedVector3 () * initialThrustFactor;

		transform.localScale = transform.localScale.normalized * scale;
		localScale = transform.localScale;

		Rigidbody rb = GetComponent<Rigidbody> ();
		rb.mass = scale * massFactor;
		rb.AddForce (initialForce);

		health = scale * healthFactor;
	}

	public override void OnStartClient ()
	{
		transform.localScale = localScale;
	}
}
