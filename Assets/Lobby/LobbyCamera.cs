﻿using UnityEngine;

public class LobbyCamera : MonoBehaviour
{
	public Vector3 rotation;

	void Update ()
	{
		transform.localRotation *= Quaternion.Euler (rotation);
	}
}
